Rails.application.routes.draw do
  resources :customers
  resources :tags
  resources :accessories
  resources :cars
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

			namespace :api do
			  namespace :v1 do
			    devise_scope :user do
			      post 'registrations' => 'registrations#create', :as => 'register'
			      put 'registrations' => 'registrations#update',  :as => 'update'
			      delete 'registrations' => 'registrations#destroy', :as => 'destroy'
			      post 'sessions' => 'sessions#create', :as => 'login'
			      delete 'sessions' => 'sessions#destroy', :as => 'logout'
			    end
			  end
			end

			namespace :api do 
				namespace :v1 do 
					  resources :accessories do 
				        collection do
				          get 'get_accessories'
				        end
				      end


				end
			end
end


