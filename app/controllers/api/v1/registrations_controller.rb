class Api::V1::RegistrationsController < ApplicationController
  skip_before_action :verify_authenticity_token,:if => Proc.new { |c| c.request.format == 'application/json' }
  # before_action :doorkeeper_authorize!


  respond_to :json

  def create
    resource = User.new(:email => params[:user][:email], :password => params[:user][:password], :password_confirmation => params[:user][:password_confirmation])
    if resource.save
      sign_in resource
      render :status => 200,:json => { :success => true,:info => "Registered",:data => { :user => resource,:auth_token => current_user.authentication_token } }
    else
      render :status => :unprocessable_entity,:json => { :success => false,:info => resource.errors,:data => {} }
    end
  end

  def update
    resource = User.find(params[:user][:id])
    if resource.update_attributes({:email => params[:user][:email],:password => params[:user][:password],:password_confirmation => params[:user][:password_confirmation]
      })
      render :status => 200,:json => { :success => true,:info => "Updated",:data => { :user => resource,:auth_token => resource.authentication_token } }
    else
      render :status => :unprocessable_entity,:json => { :success => false,:info => resource.errors,:data => {} }
    end
  end

  def destroy
    resource = User.find(params[:user][:id])
    if resource
    resource.delete
    render :status => 200,
       :json => { :success => true,:info => "Delete sucessfull !",:data => { } }
    else
      render :status => :unprocessable_entity,:json => { :success => false,:info => resource.errors,:data => {} }
    end
  end
end