json.extract! customer, :id, :name, :email, :category, :comments, :created_at, :updated_at
json.url customer_url(customer, format: :json)
