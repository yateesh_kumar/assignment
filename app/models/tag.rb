class Tag < ApplicationRecord
	has_many :accessory_tags
	has_many :accessories, through: :accessory_tags
end
