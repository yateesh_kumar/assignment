class CreateAccessoryTags < ActiveRecord::Migration[5.0]
  def change
    create_table :accessory_tags do |t|
      t.column 'accessory_id', :integer
      t.column 'tag_id', :integer

      t.timestamps
    end
  end
end
