class AccessorySerializer < ActiveModel::Serializer
  attributes  :id, :name, :car_id, :created_at, :updated_at
  has_many :tags, through: :accessory_tags
  belongs_to :car
end
