class Accessory < ApplicationRecord
	belongs_to :car
	has_many :accessory_tags
	has_many :tags, through: :accessory_tags

end
