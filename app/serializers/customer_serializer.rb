class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :category, :comments
end
