json.extract! car, :id, :name, :price, :model, :make, :created_at, :updated_at
json.url car_url(car, format: :json)
