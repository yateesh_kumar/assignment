class Api::V1::AccessoriesController < ApplicationController
	skip_before_action :verify_authenticity_token,:if => Proc.new { |c| c.request.format == 'application/json' }



  respond_to :json

  def index 
  	resource = Accessory.all
    if resource.present?
    render :status => 200,
       :json => { :success => true,:info => "list of accessories!",:data => { :accessory => resource,:auth_token => current_user.authentication_token } }
    else
      render :status => :unprocessable_entity,:json => { :success => false,:info => resource.errors, }
    end
  end

  def get_accessories
  	@accessories = Accessory.where(:car_id=>params[:accessory][:car_id]).includes(:tags).where(tags: {name: params[:accessory][:tag_name]})
  	if @accessories.present?
  		render json: @accessories, include: [:tags] ,status: 200
  	else
  		render :status => :unprocessable_entity,:json => { :success => false,:info => "404 errors", }
  	end
  end
  
end